package ${packageName};

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import ${prefixPackageName}.service.${domainObjectName}Service;
import ${prefixPackageName}.entity.${domainObjectName};
import ${prefixPackageName}.entity.${domainObjectName}Example;
import ${prefixPackageName}.entity.${domainObjectName}Example.Criteria;
import ${prefixPackageName}.mapper.${domainObjectName}Mapper;


@Service("${domainObjectNameLower}Service")
public class ${domainObjectName}ServiceImpl implements ${domainObjectName}Service {

	private final static Logger logger = LoggerFactory.getLogger(${domainObjectName}ServiceImpl.class);

	@Resource
	private ${domainObjectName}Mapper ${domainObjectNameLower}Mapper;


	@Override
	public void save${domainObjectName}(${domainObjectName} ${domainObjectNameLower}) {
		if (null == ${domainObjectNameLower}) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.${domainObjectNameLower}Mapper.insertSelective(${domainObjectNameLower});
	}

	@Override
	public void update${domainObjectName}(${domainObjectName} ${domainObjectNameLower}) {
		if (null == ${domainObjectNameLower}) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.${domainObjectNameLower}Mapper.updateByPrimaryKeySelective(${domainObjectNameLower});
	}

	@Override
	public ${domainObjectName} get${domainObjectName}ById(Integer id) {
		return ${domainObjectNameLower}Mapper.selectByPrimaryKey(id);
	}

	@Override
	public List<${domainObjectName}> get${domainObjectName}List(${domainObjectName} ${domainObjectNameLower}) {
		return this.get${domainObjectName}ListByParam(${domainObjectNameLower}, null);
	}

	@Override
	public List<${domainObjectName}> get${domainObjectName}ListByParam(${domainObjectName} ${domainObjectNameLower}, String orderByStr) {
		${domainObjectName}Example example = new ${domainObjectName}Example();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<${domainObjectName}> ${domainObjectNameLower}List = this.${domainObjectNameLower}Mapper.selectByExample(example);
		return ${domainObjectNameLower}List;
	}

	@Override
	public PageResult<${domainObjectName}> find${domainObjectName}PageByParam(${domainObjectName} ${domainObjectNameLower}, PageParam pageParam) {
		return this.find${domainObjectName}PageByParam(${domainObjectNameLower}, pageParam, null);
	}

	@Override
	public PageResult<${domainObjectName}> find${domainObjectName}PageByParam(${domainObjectName} ${domainObjectNameLower}, PageParam pageParam, String orderByStr) {
		PageResult<${domainObjectName}> pageResult = new PageResult<${domainObjectName}>();

		${domainObjectName}Example example = new ${domainObjectName}Example();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<${domainObjectName}> list = null;// ${domainObjectNameLower}Mapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	<#--@Override-->
	<#--public void delete${domainObjectName}ById(Integer id) {-->
		<#--this.${domainObjectNameLower}Mapper.deleteByPrimaryKey(id);-->
	<#--}-->

}
