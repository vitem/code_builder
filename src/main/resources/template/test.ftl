package ${packageName};

import java.util.List;
import javax.annotation.Resource;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zgg.common.page.PageParam;
import com.zgg.common.page.PageResult;
import ${prefixPackageName}.test.SpringConfigLoader;
import ${prefixPackageName}.api.${domainObjectName}Service;
import ${prefixPackageName}.bean.${domainObjectName};
@Ignore
public class ${domainObjectName}Test extends SpringConfigLoader {

	private final static Logger logger = LoggerFactory.getLogger(${domainObjectName}Test.class); 
	
	@Resource
   ${domainObjectName}Service ${domainObjectNameLower}Service;

	@Test
	public void save${domainObjectName}Test() {
		${domainObjectName} ${domainObjectNameLower} =new ${domainObjectName}();
		${domainObjectNameLower}.setId("1");
		this.${domainObjectNameLower}Service.save${domainObjectName}(${domainObjectNameLower});
	}
	
	
	@Test
	public void update${domainObjectName}Test() {
		${domainObjectName} ${domainObjectNameLower} =new ${domainObjectName}();
		${domainObjectNameLower}.setId("1");
		this.${domainObjectNameLower}Service.update${domainObjectName}(${domainObjectNameLower});
	}
	
	@Test
	public void get${domainObjectName}ByIdTest() {
		${domainObjectName} userdeom=this.${domainObjectNameLower}Service.get${domainObjectName}ById("1");
		logger.debug("id:"+userdeom.getId());
	}
	
	@Test
	public void get${domainObjectName}ListTest() {
		List<${domainObjectName}> userdemos=this.${domainObjectNameLower}Service.get${domainObjectName}List(null);
		logger.debug("userdemos.size:"+userdemos.size());
	}
	
	@Test
	public void find${domainObjectName}PageByParamTest() {
		PageParam pageParam=new PageParam();
		pageParam.setPageNo(1);
		pageParam.setPageSize(3);
		PageResult<${domainObjectName}> pageRes=this.${domainObjectNameLower}Service.find${domainObjectName}PageByParam(null, pageParam);
		logger.debug("getPageNo:"+pageRes.getPageNo());
		logger.debug("getPageSize:"+pageRes.getPageSize());
		logger.debug("getTotalCount:"+pageRes.getTotalCount());
		logger.debug("getTotalPage:"+pageRes.getTotalPage());
		logger.debug("getList:"+pageRes.getList().size());
	}
	
	@Test
	public void delete${domainObjectName}ByIdTest() {
		this.${domainObjectNameLower}Service.delete${domainObjectName}ById("1");
		logger.debug("deleteById end!");
	}
	

}
