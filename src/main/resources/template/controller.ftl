package ${packageName};

import cn.com.chinaventure.common.constant.C;
import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.common.web.ServiceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import ${prefixPackageName}.service.${domainObjectName}Service;
import ${prefixPackageName}.entity.${domainObjectName};


@RestController
@RequestMapping("/")
public class ${domainObjectName}Controller extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(${domainObjectName}Controller.class);

    @Resource
    private ${domainObjectName}Service ${domainObjectNameLower}Service;

    /**
	 * @api {post} / 新增 ${alias}
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> save${domainObjectName}(@RequestBody ${domainObjectName} ${domainObjectNameLower}) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.${domainObjectNameLower}Service.save${domainObjectName}(${domainObjectNameLower});
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改${alias}
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult update${domainObjectName}(@RequestBody ${domainObjectName} ${domainObjectNameLower}) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.${domainObjectNameLower}Service.update${domainObjectName}(${domainObjectNameLower});
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询${alias}详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<${domainObjectName}> get${domainObjectName}ById(@PathVariable String id) {
        JsonResult<${domainObjectName}> jr = new JsonResult<${domainObjectName}>();
        try {
            ${domainObjectName} ${domainObjectNameLower}  = ${domainObjectNameLower}Service.get${domainObjectName}ById(id);
            jr = buildJsonResult(${domainObjectNameLower});
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /${domainObjectNameLower}/get${domainObjectName}List 查询${alias}列表(不带分页)
	 * @apiGroup ${alias}管理
	 * @apiName 查询${alias}列表(不带分页)
	 * @apiDescription 根据条件查询${alias}列表(不带分页)
	 * @apiParam {${domainObjectName}} ${domainObjectNameLower} ${alias}对象
	 */
    @RequestMapping("/get${domainObjectName}List")
    public JsonResult<List<${domainObjectName}>> get${domainObjectName}List(${domainObjectName} ${domainObjectNameLower}) {
        JsonResult<List<${domainObjectName}>> jr = new JsonResult<List<${domainObjectName}>>();
        try {
            List<${domainObjectName}> list = this.${domainObjectNameLower}Service.get${domainObjectName}List(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /${domainObjectNameLower}/find${domainObjectName}PageByParam 查询${alias}列表(带分页)
	 * @apiGroup ${alias}管理
	 * @apiName 查询${alias}列表(带分页)
	 * @apiDescription 根据条件查询${alias}列表(带分页)
	 * @apiParam {${domainObjectName}} ${domainObjectNameLower} ${alias}对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/find${domainObjectName}PageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<${domainObjectName}>> find${domainObjectName}PageByParam(@RequestBody ${domainObjectName} ${domainObjectNameLower}, PageParam pageParam) {
        JsonResult<PageResult<${domainObjectName}>> jr = new JsonResult<PageResult<${domainObjectName}>>();
        try {
            PageResult<${domainObjectName}> page = this.${domainObjectNameLower}Service.find${domainObjectName}PageByParam(${domainObjectNameLower}, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /${domainObjectNameLower}/delete${domainObjectName}ById 删除${alias}(物理删除)
	 * @apiGroup ${alias}管理
	 * @apiName 删除${alias}记录(物理删除)
	 * @apiDescription 根据主键id删除${alias}记录(物理删除)
	 * @apiParam {String} id ${alias}主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/delete${domainObjectName}ById",method=RequestMethod.DELETE)
    public JsonResult delete${domainObjectName}ById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.${domainObjectNameLower}Service.delete${domainObjectName}ById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
