package ${packageName};

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zgg.common.GeneratorKey;
import com.zgg.common.ServiceResult;
import com.zgg.common.AbstractServiceResult;
import com.zgg.common.code.ErrCodeE;
import com.zgg.common.page.PageParam;
import com.zgg.common.page.PageResult;
import ${prefixPackageName}.api.${domainObjectName}Service;
import ${prefixPackageName}.bean.${domainObjectName};

/**
 * @author
 */
@Service("${domainObjectNameLower}ServicePretend")
public class ${domainObjectName}ServicePretend  extends AbstractServiceResult{

    private final static Logger logger = LoggerFactory.getLogger(${domainObjectName}ServicePretend.class);

    @Autowired
    private ${domainObjectName}Service ${domainObjectNameLower}Service;

    /**
     * 新增
     *
     * @param ${domainObjectNameLower}
     */
    public ServiceResult<Object> save${domainObjectName}(${domainObjectName} ${domainObjectNameLower}) {
        ServiceResult<Object> sr = new ServiceResult<Object>();
        try {
            ${domainObjectNameLower}.setId(GeneratorKey.genaraId());
            ${domainObjectNameLower}Service.save${domainObjectName}(${domainObjectNameLower});
            sr = buildSuccessResult(${domainObjectNameLower});
        } catch (Exception e) {
            logger.error("error", e);
            sr = buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
        return sr;
    }


    /**
     * 修改
     *
     * @param ${domainObjectNameLower}
     */
    public ServiceResult<Object> update${domainObjectName}(${domainObjectName} ${domainObjectNameLower}) {
        ServiceResult<Object> sr = new ServiceResult<Object>();
        try {
            ${domainObjectNameLower}Service.update${domainObjectName}(${domainObjectNameLower});
            sr = buildSuccessResult(${domainObjectNameLower});
        } catch (Exception e) {
            logger.error("error", e);
            sr = buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
        return sr;
    }

    /**
     * 新增或修改
     *
     * @param ${domainObjectNameLower}
     */
    public ServiceResult<Object> saveOrUpdate${domainObjectName}(${domainObjectName} ${domainObjectNameLower}) {
        ServiceResult<Object> sr = new ServiceResult<Object>();
        if (null == ${domainObjectNameLower}) {
            sr = buildBusinessFailResult(ErrCodeE.PARAM_INVALID);
            return sr;
        }
        try {
            if (StringUtils.isBlank(${domainObjectNameLower}.getId())) {
                this.${domainObjectNameLower}Service.save${domainObjectName}(${domainObjectNameLower});
            } else {
                this.${domainObjectNameLower}Service.update${domainObjectName}(${domainObjectNameLower});
            }
            sr = buildSuccessResult(${domainObjectNameLower});
        } catch (Exception e) {
            logger.error("error", e);
            sr = buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
        return sr;
    }


    public ServiceResult<${domainObjectName}> get${domainObjectName}ById(String id) {
        try {
            ${domainObjectName} ${domainObjectNameLower} = ${domainObjectNameLower}Service.get${domainObjectName}ById(id);
            return buildSuccessResult(${domainObjectNameLower});
        } catch (Exception e) {
            logger.error("error", e);
            return buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
    }



    public ServiceResult<Object> delete${domainObjectName}ById(String id) {
        try {
            ${domainObjectNameLower}Service.delete${domainObjectName}ById(id);
            return buildSuccessResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            return buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
    }


    public ServiceResult<List<${domainObjectName}>> get${domainObjectName}List(${domainObjectName} ${domainObjectNameLower}) {
        try {
            List<${domainObjectName}> list = ${domainObjectNameLower}Service.get${domainObjectName}List(${domainObjectNameLower});
            return buildSuccessResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            return buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
    }


    public ServiceResult<PageResult<${domainObjectName}>> find${domainObjectName}PageByParam(${domainObjectName} ${domainObjectNameLower}, PageParam pageParam) {
        try {
            PageResult<${domainObjectName}> pageResult = ${domainObjectNameLower}Service.find${domainObjectName}PageByParam(${domainObjectNameLower}, pageParam);
            return buildSuccessResult(pageResult);
        } catch (Exception e) {
            logger.error("error", e);
            return buildBusinessFailResult(e,ErrCodeE.DB_ERROR);
        }
    }

}
