package ${packageName};

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import ${prefixPackageName}.entity.${domainObjectName};

/**
 * @author 
 *
 */
public interface ${domainObjectName}Service {
	
	/**
     * @api save${domainObjectName} 新增${alias}
     * @apiGroup ${alias}管理
     * @apiName  新增${alias}记录
     * @apiDescription 全量插入${alias}记录
     * @apiParam {${domainObjectName}} ${domainObjectNameLower} ${alias}对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void save${domainObjectName} (${domainObjectName} ${domainObjectNameLower});
	
	/**
     * @api update${domainObjectName} 修改${alias}
     * @apiGroup ${alias}管理
     * @apiName  修改${alias}记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {${domainObjectName}} ${domainObjectNameLower} ${alias}对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void update${domainObjectName}(${domainObjectName} ${domainObjectNameLower});
	
	/**
     * @api get${domainObjectName}ById 根据${alias}id查询详情
     * @apiGroup ${alias}管理
     * @apiName  查询${alias}详情
     * @apiDescription 根据主键id查询${alias}详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess ${domainObjectName} ${alias}实体对象
     * @apiVersion 1.0.0
     * 
     */
	public ${domainObjectName} get${domainObjectName}ById(Integer id);
	
	/**
     * @api get${domainObjectName}List 根据${alias}条件查询列表
     * @apiGroup ${alias}管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {${domainObjectName}} ${domainObjectNameLower}  实体条件
     * @apiSuccess List<${domainObjectName}> ${alias}实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<${domainObjectName}> get${domainObjectName}List(${domainObjectName} ${domainObjectNameLower});
	

	/**
     * @api get${domainObjectName}ListByParam 根据${alias}条件查询列表（含排序）
     * @apiGroup ${alias}管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {${domainObjectName}} ${domainObjectNameLower}  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<${domainObjectName}> ${alias}实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<${domainObjectName}> get${domainObjectName}ListByParam(${domainObjectName} ${domainObjectNameLower}, String orderByStr);
	
	
	/**
     * @api find${domainObjectName}PageByParam 根据${alias}条件查询列表（分页）
     * @apiGroup ${alias}管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {${domainObjectName}} ${domainObjectNameLower}  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<${domainObjectName}> ${alias}实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<${domainObjectName}> find${domainObjectName}PageByParam(${domainObjectName} ${domainObjectNameLower}, PageParam pageParam); 
	
	/**
     * @api find${domainObjectName}PageByParam 根据${alias}条件查询列表（分页，含排序）
     * @apiGroup ${alias}管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {${domainObjectName}} ${domainObjectNameLower}  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<${domainObjectName}> ${alias}实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<${domainObjectName}> find${domainObjectName}PageByParam(${domainObjectName} ${domainObjectNameLower}, PageParam pageParam,String orderByStr); 
	
	
	<#--/**-->
     <#--* @api delete${domainObjectName}ById 根据${alias}的id删除(物理删除)-->
     <#--* @apiGroup ${alias}管理-->
     <#--* @apiName  根据id删除(物理删除)-->
     <#--* @apiDescription 根据主键id删除对象(物理删除)-->
     <#--* @apiParam {Integer} id  主键id-->
     <#--* @apiSuccess void-->
     <#--* @apiVersion 1.0.0-->
     <#--* -->
     <#--*/-->
	<#--public void delete${domainObjectName}ById(Integer id);-->

}

