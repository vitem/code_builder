package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.Agency;
import cn.com.chinaventure.trans.datacenter.entity.AgencyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AgencyMapper {
    int countByExample(AgencyExample example);

    int deleteByExample(AgencyExample example);

    int deleteByPrimaryKey(String id);

    int insert(Agency record);

    int insertSelective(Agency record);

    List<Agency> selectByExampleWithRowbounds(AgencyExample example, RowBounds rowBounds);

    List<Agency> selectByExample(AgencyExample example);

    Agency selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Agency record, @Param("example") AgencyExample example);

    int updateByExample(@Param("record") Agency record, @Param("example") AgencyExample example);

    int updateByPrimaryKeySelective(Agency record);

    int updateByPrimaryKey(Agency record);
}