package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.util.Date;

public class Agency implements Serializable {
    /**
     * ID
     */
    @ApiModelProperty(notes = "ID",)
    private String id;

    /**
     * 工商ID
     */
    @ApiModelProperty(notes = "工商ID",)
    private String companyId;

    /**
     * 中介机构名称
     */
    @ApiModelProperty(notes = "中介机构名称",)
    private String agencyName;

    /**
     * 中介机构类型： 1投行、2律所、3会所、4评估公司、5托管银行、6交易所信息
     */
    @ApiModelProperty(notes = "中介机构类型： 1投行、2律所、3会所、4评估公司、5托管银行、6交易所信息",)
    private Integer agencyType;

    /**
     * 营业状态：1营业、2停业  
     */
    @ApiModelProperty(notes = "营业状态：1营业、2停业  ",)
    private Integer businessStatus;

    /**
     * 审核状态：1已审核、2未审核
     */
    @ApiModelProperty(notes = "审核状态：1已审核、2未审核",)
    private Integer verifyStatus;

    /**
     * 审核时间
     */
    @ApiModelProperty(notes = "审核时间",)
    private Date verifyTime;

    /**
     * 审核人
     */
    @ApiModelProperty(notes = "审核人",)
    private String verifyUserId;

    /**
     * 备注
     */
    @ApiModelProperty(notes = "备注",)
    private String remark;

    /**
     * 是否在发改委备案：1是；2否
     */
    @ApiModelProperty(notes = "是否在发改委备案：1是；2否",)
    private Integer ndrcRecordType;

    /**
     * 业务类型：1上市承销商 2：新型投行
     */
    @ApiModelProperty(notes = "业务类型：1上市承销商 2：新型投行",)
    private Integer businessType;

    /**
     * 显示权限：1公开、2:半公开、3:未公开
     */
    @ApiModelProperty(notes = "显示权限：1公开、2:半公开、3:未公开",)
    private Integer showPermission;

    /**
     * 修改人
     */
    @ApiModelProperty(notes = "修改人",)
    private String createrId;

    /**
     * 创建时间
     */
    @ApiModelProperty(notes = "创建时间",)
    private Date createTime;

    /**
     * 修改人
     */
    @ApiModelProperty(notes = "修改人",)
    private String modifierId;

    /**
     * 修改时间
     */
    @ApiModelProperty(notes = "修改时间",)
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    @ApiModelProperty(notes = "1：正常  2删除 3...",)
    private Integer status;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName == null ? null : agencyName.trim();
    }

    public Integer getAgencyType() {
        return agencyType;
    }

    public void setAgencyType(Integer agencyType) {
        this.agencyType = agencyType;
    }

    public Integer getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(Integer businessStatus) {
        this.businessStatus = businessStatus;
    }

    public Integer getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(Integer verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public String getVerifyUserId() {
        return verifyUserId;
    }

    public void setVerifyUserId(String verifyUserId) {
        this.verifyUserId = verifyUserId == null ? null : verifyUserId.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getNdrcRecordType() {
        return ndrcRecordType;
    }

    public void setNdrcRecordType(Integer ndrcRecordType) {
        this.ndrcRecordType = ndrcRecordType;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getShowPermission() {
        return showPermission;
    }

    public void setShowPermission(Integer showPermission) {
        this.showPermission = showPermission;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", companyId=").append(companyId);
        sb.append(", agencyName=").append(agencyName);
        sb.append(", agencyType=").append(agencyType);
        sb.append(", businessStatus=").append(businessStatus);
        sb.append(", verifyStatus=").append(verifyStatus);
        sb.append(", verifyTime=").append(verifyTime);
        sb.append(", verifyUserId=").append(verifyUserId);
        sb.append(", remark=").append(remark);
        sb.append(", ndrcRecordType=").append(ndrcRecordType);
        sb.append(", businessType=").append(businessType);
        sb.append(", showPermission=").append(showPermission);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}