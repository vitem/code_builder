package cn.com.chinaventure.trans.datacenter.controller;

import cn.com.chinaventure.common.constant.C;
import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.common.web.ServiceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.AgencyService;
import cn.com.chinaventure.trans.datacenter.entity.Agency;


@RestController
@RequestMapping("/agency")
public class AgencyController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(AgencyController.class);

    @Resource
    private AgencyService agencyService;

    /**
	 * @api {post} / 新增agency
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveAgency(@RequestBody Agency agency) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.agencyService.saveAgency(agency);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改agency
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateAgency(@RequestBody Agency agency) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.agencyService.updateAgency(agency);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询agency详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Agency> getAgencyById(@PathVariable String id) {
        JsonResult<Agency> jr = new JsonResult<Agency>();
        try {
            Agency agency  = agencyService.getAgencyById(id);
            jr = buildJsonResult(agency);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /agency/getAgencyList 查询agency列表(不带分页)
	 * @apiGroup agency管理
	 * @apiName 查询agency列表(不带分页)
	 * @apiDescription 根据条件查询agency列表(不带分页)
	 * @apiParam {Agency} agency agency对象
	 */
    @RequestMapping("/getAgencyList")
    public JsonResult<List<Agency>> getAgencyList(Agency agency) {
        JsonResult<List<Agency>> jr = new JsonResult<List<Agency>>();
        try {
            List<Agency> list = this.agencyService.getAgencyList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /agency/findAgencyPageByParam 查询agency列表(带分页)
	 * @apiGroup agency管理
	 * @apiName 查询agency列表(带分页)
	 * @apiDescription 根据条件查询agency列表(带分页)
	 * @apiParam {Agency} agency agency对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findAgencyPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Agency>> findAgencyPageByParam(@RequestBody Agency agency, PageParam pageParam) {
        JsonResult<PageResult<Agency>> jr = new JsonResult<PageResult<Agency>>();
        try {
            PageResult<Agency> page = this.agencyService.findAgencyPageByParam(agency, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /agency/deleteAgencyById 删除agency(物理删除)
	 * @apiGroup agency管理
	 * @apiName 删除agency记录(物理删除)
	 * @apiDescription 根据主键id删除agency记录(物理删除)
	 * @apiParam {String} id agency主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteAgencyById",method=RequestMethod.DELETE)
    public JsonResult deleteAgencyById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.agencyService.deleteAgencyById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
