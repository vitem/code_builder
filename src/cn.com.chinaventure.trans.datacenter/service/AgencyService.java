package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Agency;

/**
 * @author 
 *
 */
public interface AgencyService {
	
	/**
     * @api saveAgency 新增agency
     * @apiGroup agency管理
     * @apiName  新增agency记录
     * @apiDescription 全量插入agency记录
     * @apiParam {Agency} agency agency对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveAgency (Agency agency);
	
	/**
     * @api updateAgency 修改agency
     * @apiGroup agency管理
     * @apiName  修改agency记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Agency} agency agency对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateAgency(Agency agency);
	
	/**
     * @api getAgencyById 根据agencyid查询详情
     * @apiGroup agency管理
     * @apiName  查询agency详情
     * @apiDescription 根据主键id查询agency详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Agency agency实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Agency getAgencyById(Integer id);
	
	/**
     * @api getAgencyList 根据agency条件查询列表
     * @apiGroup agency管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Agency} agency  实体条件
     * @apiSuccess List<Agency> agency实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Agency> getAgencyList(Agency agency);
	

	/**
     * @api getAgencyListByParam 根据agency条件查询列表（含排序）
     * @apiGroup agency管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Agency} agency  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Agency> agency实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Agency> getAgencyListByParam(Agency agency, String orderByStr);
	
	
	/**
     * @api findAgencyPageByParam 根据agency条件查询列表（分页）
     * @apiGroup agency管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Agency} agency  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Agency> agency实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Agency> findAgencyPageByParam(Agency agency, PageParam pageParam); 
	
	/**
     * @api findAgencyPageByParam 根据agency条件查询列表（分页，含排序）
     * @apiGroup agency管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Agency} agency  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Agency> agency实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Agency> findAgencyPageByParam(Agency agency, PageParam pageParam,String orderByStr); 
	
	

}

