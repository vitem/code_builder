package cn.com.chinaventure.trans.datacenter.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.AgencyService;
import cn.com.chinaventure.trans.datacenter.entity.Agency;
import cn.com.chinaventure.trans.datacenter.entity.AgencyExample;
import cn.com.chinaventure.trans.datacenter.entity.AgencyExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.AgencyMapper;


@Service("agencyService")
public class AgencyServiceImpl implements AgencyService {

	private final static Logger logger = LoggerFactory.getLogger(AgencyServiceImpl.class);

	@Resource
	private AgencyMapper agencyMapper;


	@Override
	public void saveAgency(Agency agency) {
		if (null == agency) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.agencyMapper.insertSelective(agency);
	}

	@Override
	public void updateAgency(Agency agency) {
		if (null == agency) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.agencyMapper.updateByPrimaryKeySelective(agency);
	}

	@Override
	public Agency getAgencyById(Integer id) {
		return agencyMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Agency> getAgencyList(Agency agency) {
		return this.getAgencyListByParam(agency, null);
	}

	@Override
	public List<Agency> getAgencyListByParam(Agency agency, String orderByStr) {
		AgencyExample example = new AgencyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Agency> agencyList = this.agencyMapper.selectByExample(example);
		return agencyList;
	}

	@Override
	public PageResult<Agency> findAgencyPageByParam(Agency agency, PageParam pageParam) {
		return this.findAgencyPageByParam(agency, pageParam, null);
	}

	@Override
	public PageResult<Agency> findAgencyPageByParam(Agency agency, PageParam pageParam, String orderByStr) {
		PageResult<Agency> pageResult = new PageResult<Agency>();

		AgencyExample example = new AgencyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Agency> list = null;// agencyMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
